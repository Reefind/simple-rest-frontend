json = `
    {
        "apiUrls": [
            "simple-rest-frontend.com",
            "simple-rest-frontend-bis.com",
            "simple-rest-frontend-calls.com"
        ],
        "tabs": [
            {
                "title": "Actions",
                "content": [
                    {
                        "title": "Action 1",
                        "description": "Description 1",
                        "path": "/test1",
                        "parameters": [
                            ["Parameter 1", "choix 1", "choix 2", "choix 3"],
                            "Parameter 2",
                            "Parameter 3",
                            "Parameter 4"
                        ],
                        "color": "#6E6ED7"
                    },
                    {
                        "title": "Action 2",
                        "description": "Description 2",
                        "path": "@0/test2",
                        "parameters": [
                            ["Parameter 1", "choix 1", "choix 2", "choix 3"],
                            "Parameter 2",
                            "Parameter 3"
                        ],
                        "color": "red"
                    },
                    {
                        "title": "Action 3",
                        "description": "Description 3",
                        "path": "@1/test3",
                        "parameters": [
                            ["Parameter 1", "choix 1", "choix 2", "choix 3", "choix 4"],
                            "Parameter 2",
                            "Parameter 3"
                        ]
                    },
                    {
                        "title": "Action 4",
                        "description": "Description 4",
                        "path": "/test4/test",
                        "parameters": [
                            ["Parameter 1", "choix 1", "choix 2", "choix 3", "choix 4"]
                        ]
                    },
                    {
                        "title": "Action 5",
                        "description": "Description 5",
                        "path": "/test5",
                        "parameters": [
                            ["Parameter 1", "choix 1", "choix 2", "choix 3", "choix 4"],
                            "Parameter 2",
                            "Parameter 3"
                        ],
                        "color": "#6be3b7"
                    }
                ]
            },
            {
                "title": "Calls",
                "hasOrder": true,
                "content": [
                    {
                        "title": "Call 1",
                        "description": "Description 1",
                        "path": "@2/test1",
                        "parameters": [
                            ["Parameter 1", "choix 1", "choix 2", "choix 3"],
                            "Parameter 2",
                            "Parameter 3",
                            "Parameter 4"
                        ]
                    },
                    {
                        "title": "Call 2",
                        "description": "Description 2",
                        "path": "@2/test2",
                        "parameters": [
                            ["Parameter 1", "choix 1", "choix 2", "choix 3"],
                            "Parameter 2",
                            "Parameter 3"
                        ]
                    },
                    {
                        "title": "Call 3",
                        "description": "Description 3",
                        "path": "@2/test3",
                        "parameters": [
                            ["Parameter 1", "choix 1", "choix 2", "choix 3", "choix 4"],
                            "Parameter 2",
                            "Parameter 3"
                        ]
                    },
                    {
                        "title": "Call 4",
                        "description": "Description 4",
                        "path": "@2/test4/test",
                        "parameters": [
                            ["Parameter 1", "choix 1", "choix 2", "choix 3", "choix 4"]
                        ]
                    },
                    {
                        "title": "Call 5",
                        "description": "Description 5",
                        "path": "@2/test5",
                        "parameters": [
                            ["Parameter 1", "choix 1", "choix 2", "choix 3", "choix 4"],
                            "Parameter 2",
                            "Parameter 3"
                        ]
                    }
                ]
            }
        ]
    }
`;
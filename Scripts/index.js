
var data = JSON.parse(json);
var apiAddresses = data["apiUrls"];
var hasOrder = data["tabs"][1]["hasOrder"];

var tabs = data["tabs"];
var currentTab = 0;

//Tab button template
var tabButtonHTML = '<button class="tab_links@tab_active" onclick="showTab(event, \'@content_id\', @index)">@tab_title</button>';

//Tab content template
var tabContentHTML = '<div id="@content_id" class="tab_content"></div>';

//Action card template
var actionCardHTML = '<div class="@class" id="@action_id"><div class="action_illustration" style="background-color:@action_color"></div><h3 class="action_card_title">@action_title</h3><p><i>@action_description</i></p></div>'

//Parameter Field Input
var parameterFieldHTML = '<div class="parameter_container"><label for="@parameter_id">@parameter_name :</label><input type="text" id="@parameter_id" name="@parameter_name" required/></div>';

var selectFieldHTML = '<div class="parameter_select_container"><label for="@parameter_select_id">@parameter_name :</label><select id="@parameter_select_id">@parameter_select_content</select></div>';

//Wait for the HTML page to be rendered
$( document ).ready(function() {
    init();
});

function init() {

    //Set up UI event listeners
    $("#window_filter").on("click", function() {
        $(this).css("display", "none");
        $("#action_window").css("display", "none");
    });

    //Initialize the tabs buttons
    for(var i = 0; i < tabs.length; i++) {
        $("#side_panel").append(getTabButton(i));
    }

    //Initialize the tabs contents
    for(var i = 0; i < tabs.length; i++) {
        $("#content_container").append(getTabContent(i));
        if(i == 0) {
            $('#' + tabs[0]["title"].charAt(0).toLowerCase() + tabs[0]["title"].slice(1) + "_content").css("display", "grid");
        }
    }

    //Initialize the cards based on the API's card paths and tabs
    for(var j = 0; j < tabs.length; j++) {
        for(var i = 0; i < tabs[j]["content"].length; i++) {
            var hasOrder = tabs[j]["hasOrder"];
            if(hasOrder) {
                if(i == 0) {
                    $('#' + tabs[j]["title"].charAt(0).toLowerCase() + tabs[j]["title"].slice(1) + "_content").append(getCard(j, i, true));
                    document.getElementById(tabs[j]["title"].charAt(0).toLowerCase() + tabs[j]["title"].slice(1) + "_card_" + i).addEventListener("click", openActionWindow);
                    document.getElementById(tabs[j]["title"].charAt(0).toLowerCase() + tabs[j]["title"].slice(1) + "_card_" + i).index = i;
                } else {
                    $('#' + tabs[j]["title"].charAt(0).toLowerCase() + tabs[j]["title"].slice(1) + "_content").append(getCard(j, i, false));
                }
            } else {
                $('#' + tabs[j]["title"].charAt(0).toLowerCase() + tabs[j]["title"].slice(1) + "_content").append(getCard(j, i, true));
                document.getElementById(tabs[j]["title"].charAt(0).toLowerCase() + tabs[j]["title"].slice(1) + "_card_" + i).addEventListener("click", openActionWindow);
                document.getElementById(tabs[j]["title"].charAt(0).toLowerCase() + tabs[j]["title"].slice(1) + "_card_" + i).index = i;
            }
        }
    }
}

//Create a tab button based on the configuration file
function getTabButton(index) {
    var html = tabButtonHTML;

    if(index == 0) {
        html = html.replace("@tab_active", " active");
    } else {
        html = html.replace("@tab_active", "");
    }

    html = html.replace("@tab_title", tabs[index]["title"]);
    html = html.replace("@content_id", tabs[index]["title"].charAt(0).toLowerCase() + tabs[index]["title"].slice(1) + "_content");
    html = html.replace("@index", index);

    return html;
}

//Generate a tab content container
function getTabContent(index) {
    var html = tabContentHTML;

    html = html.replace("@content_id", tabs[index]["title"].charAt(0).toLowerCase() + tabs[index]["title"].slice(1) + "_content");

    return html;
}

//Create an action card by replacing all the fields of the action card template
function getCard(tabIndex, cardIndex, active) {
    var html = actionCardHTML;

    if(active) {
        html = html.replace("@class", "action_card");
    } else {
        html = html.replace("@class", "action_card_inactive");
    }

    html = html.replace("@action_id", tabs[tabIndex]["title"].charAt(0).toLowerCase() + tabs[tabIndex]["title"].slice(1) + "_card_" + cardIndex);
    var color = tabs[tabIndex]["content"][cardIndex]["color"];
    if(color == undefined || color == "") {
        html = html.replace("@action_color", "#6E6ED7");
    } else {
        html = html.replace("@action_color", color);
    }
    html = html.replace("@action_title", tabs[tabIndex]["content"][cardIndex]["title"]);
    html = html.replace("@action_description", tabs[tabIndex]["content"][cardIndex]["description"]);

    return html;
}

//Create a parameter field according to the selected action
function getParameterField(actionIndex, index) {
    var parameter = tabs[currentTab]["content"][actionIndex]["parameters"][index];
    if(typeof parameter == "string") {
        var html = parameterFieldHTML;
        html = html.split("@parameter_name").join(parameter);
        html = html.split("@parameter_id").join("parameter" + index);
    } else {
        var html = selectFieldHTML;
        html = html.replace("@parameter_name", parameter[0]);
        html = html.split("@parameter_select_id").join("parameter" + index);
        selectContent = "";
        for(var i = 1; i < parameter.length; i++) {
            selectContent += '<option value="' + parameter[i] + '">' + parameter[i] + '</option>';
        }

        html = html.replace("@parameter_select_content", selectContent);
    }

    return html
}

//Open the action window and set the right title and description
function openActionWindow(event) {

    var index = event.target.index;

    //This check is necessary as clicking on child elements of the card triggers the function without passing the index
    if(index == undefined) {
        index = event.target.parentElement.index;
        if(index == undefined) {
            index = event.target.parentElement.parentElement.index;
        }
    }

    $("#window_filter").css("display", "block");
    $("#action_window").css("display", "block");

    $("#action_window_title").html(tabs[currentTab]["content"][index]["title"]);
    $("#action_window_description").html(tabs[currentTab]["content"][index]["description"]);

    var parametersCount = tabs[currentTab]["content"][index]["parameters"].length;

    $('#action_window_content').html("");

    for(var i = 0; i < parametersCount; i++) {
        $('#action_window_content').append(getParameterField(index, i));
    }

    document.getElementById("action_submit").setAttribute("onClick", "javascript: submitAction(" + index + ',' + parametersCount + ");");
}

//Get all the parameters to form the target url
function submitAction(index, parametersCount) {
    var path = tabs[currentTab]["content"][index]["path"];
    var url = "";
    if(path[0] == '@') {
        url = apiAddresses[parseInt(path[1])];
    } else if(path[0] == '/') {
        url = apiAddresses[0] + path;
    } else {
        url = path;
    }

    for(var i = 0; i < parametersCount; i++) {
        if(i == 0) {
            url += '/' + document.getElementById("parameter" + i).value;
        } else {
            url += ',' + document.getElementById("parameter" + i).value;
        }
    }

    callApi(url, index);
}

function callApi(url, cardIndex) {
    alert("Calling : " + url);

    const apiCall = async () => {
        const response = await fetch(url);
        const json = await response.json();

        return json;
    }

    //alert(apiCall);

    var nextCardId = tabs[currentTab]["title"].charAt(0).toLowerCase() + tabs[currentTab]["title"].slice(1) + "_card_" + (cardIndex + 1);
    
    var nextCard = document.getElementById(nextCardId);

    if(nextCard != null) {
        nextCard.className = "action_card";
        nextCard.addEventListener("click", openActionWindow);
        nextCard.index = cardIndex + 1;
    }

    $("#window_filter").css("display", "none");
    $("#action_window").css("display", "none");
}

function showTab(evt, tabId, tabIndex) {
    var i, tabcontent, tablinks;
  
    tabcontent = document.getElementsByClassName("tab_content");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
  
    tablinks = document.getElementsByClassName("tab_links");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
  
    document.getElementById(tabId).style.display = "grid";
    evt.currentTarget.className += " active";

    currentTab = tabIndex;
  }